import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from 'src/app/globals';

@Injectable({
  providedIn: 'root'
})
export class ParameterService {

  constructor(private http: HttpClient) { }
  
  // get all parameters
  getAllParameters() {
    return this.http.get(Globals.url + '/parameter/getAllParameters');
  }

  // save general parameter
  saveParameter(parameter:any) {
    return this.http.post(Globals.url + '/parameter/saveParameter', parameter);
  }


  getParametersFiltered(parameterModel:any){
    return this.http.post(Globals.url+'/parameter/getFiltered',parameterModel)
  }
  deleteParameterById(elementId:number){
    return this.http.post(Globals.url+'/parameter/deleteElement',elementId)
  }

}
