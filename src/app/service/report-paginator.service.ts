import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ReportPaginatorService {

  constructor(private http: HttpClient) { }

  getSomeSchedulers(pageSize:number, pageIndex:number  ){
    return this.http.get(Globals.url+"/reportPaginator/getSomeScheduler/"+pageSize+"/"+pageIndex);
  }

  getCount(){
    return this.http.get(Globals.url+"/reportPaginator/getCount");
  }

  
  getParametersFiltered2(pageSize:number,pageIndex:number,scheduler:any){
    return this.http.post(Globals.url+'/reportPaginator/getFiltered2/'+pageSize+"/"+pageIndex,scheduler);
  }

  getCount2(filterObject:any){  
    return this.http.post(Globals.url+"/reportPaginator/getCount2",filterObject);
  }
  getReports(){
    return this.http.get(Globals.url+"/report/getAll");
  }
  getFilteredReports(filterObject:any,pageSize:any,pageIndex:any){
    return this.http.post(Globals.url+"/report/getFilteredReports/"+pageSize+"/"+pageIndex,filterObject);
  }
  getSizeofReport(filterObject:any){
    return this.http.post(Globals.url+"/report/getSizeofReport", filterObject);
  }
  getFailedTestCases(report:any ){
    return this.http.post(Globals.url+"/report/getFailedTestCases/",report);
  }

}
