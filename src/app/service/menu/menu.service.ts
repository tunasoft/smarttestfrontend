import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Globals } from 'src/app/globals';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  requestOptions: { params: any; };
  constructor(private http: HttpClient) { 

    this.requestOptions = {
      params: new HttpParams()
    };    this.requestOptions.params.set('BARBAROS', '1');
  
   
     
  }

  // get left menu items
  getMenuItems() {
    var config = {
      params: {username: "BARBAROS", password:"1"},
      headers : {'Accept' : 'application/json'}
     };
    return this.http.get(Globals.url + '/menu/menuItems');
  }

  // get left sub menu items
  getSubMenuItems() {
    return this.http.get(Globals.url + '/menu/subMenuItems',this.requestOptions);
  }

  // get single menu item by path
  getMenuItemByPath(path:String) {
    return this.http.get(Globals.url + '/menu/getItemByPath/' + path,this.requestOptions);
  }

  // get language parameters
  getLanguageParameters(language:String) {
    return this.http.get(Globals.url + '/menu/getLanguageParameters/' + language,this.requestOptions);
  }

  loadLanguageParameters(){    
    this.getLanguageParameters(Globals.language).subscribe((data)=>{
      Globals.languageArray = data;
    });
  }
​
  // show language item
  showLanguageItem(name:String){
    if (Globals.languageArray == null || typeof(Globals.languageArray) == undefined){
      return "";
    }
    return Globals.languageArray["" + name].name;
  }
}
