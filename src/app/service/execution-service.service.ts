import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from 'src/app/globals';
@Injectable({
  providedIn: 'root'
})
export class ExecutionServiceService {

  constructor(private http: HttpClient) { 
    
  }

    
addScheduler(scheduler:any, selectedTestCases:any) {
  let params:any={
    "scheduler":scheduler,
    "selectedTestCases": selectedTestCases
  };
  return this.http.post(Globals.url + '/execution/addScheduler', params);
}

test(){
  return this.http.post(Globals.url + '/junit/test',null);
}
create(){
  return this.http.post(Globals.url+ '/junit/create',null);
}
run(scheduler:any){
  return this.http.post(Globals.url+ '/junit/run',scheduler);
}
run2(scheduler:any){
  return this.http.post(Globals.url+ '/junit/run2',scheduler);
}
getSrc(scheduler:any)
{
  return this.http.post(Globals.url+'/junit/getSrc',scheduler);
}
showImages(schedulerId:any){
 return this.http.get(Globals.url+'/execution/showImages/'+schedulerId);

}
getImage2(url:any){
  return this.http.post(Globals.url+'/execution/getImage',url)
}
getImage(){
  return this.http.get(Globals.url+'/execution/Image')
} 

clear(){
  return this.http.get(Globals.url+'/report/clear')

}


getTentativeReports(){
  return this.http.get(Globals.url+'/report/getTentativeReports')

}
}
