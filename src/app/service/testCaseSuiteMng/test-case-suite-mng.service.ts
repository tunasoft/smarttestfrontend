import { Injectable } from '@angular/core';
import { Globals } from 'src/app/globals';
import { HttpClient } from '@angular/common/http';
import { Glob } from 'glob';

@Injectable({
  providedIn: 'root'
})
export class TestCaseSuiteMngService {

  constructor(private http: HttpClient) { }
  
getRoot() {
  return this.http.get(Globals.url + '/testCaseSuiteManagement/getRoot');
}
getFoldersByParent(id:number) {
  return this.http.get(Globals.url + '/testCaseSuiteManagement/getFolders/'+id);
}
getTestCase(id:number) {
  return this.http.get(Globals.url + '/testCaseSuiteManagement/getTestCase/'+id);
}

getTestCases() {
  return this.http.get(Globals.url + '/testCaseSuiteManagement/getTestCases/');
}


saveTestCase(testCase:any){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/saveTestCase',testCase);
}

saveFile(folder:any, id:number){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/saveFile/'+id,folder);

}

deleteFile(fileId:any){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/deleteFile',fileId);
}
addTestStep(testStep:any, testCaseId:number){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/addTestStep/'+testCaseId,testStep);
}
getTestSteps(testCaseId:number){
  return this.http.get(Globals.url+'/testCaseSuiteManagement/getTestStepsByTestCase/'+testCaseId);
}
setOrder(testStepArray:any)
{
  return this.http.post(Globals.url+'/testCaseSuiteManagement/getData',testStepArray);
}
deleteStep(testStep:any)
{
  return this.http.post(Globals.url+'/testCaseSuiteManagement/deleteStep',testStep);
}

saveText(testSteps:any){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/setText',testSteps);
}

getTestCaseById(testCaseId:any){
  return this.http.get(Globals.url+'/testCaseSuiteManagement/getTestCase2/'+testCaseId);
}
saveTestCasesasStep(testCaseId:any, selectedTestCaseId:any){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/saveTestCasesasStep/'+selectedTestCaseId,testCaseId);
}

getTemplatedTestCases(){
  return this.http.get(Globals.url+'/testCaseSuiteManagement/getTemplatedTestCases/');
}

saveJavaCodeasStep(javaCode:any, selectedTestCaseId:any){
  return this.http.post(Globals.url+'/testCaseSuiteManagement/saveJavaCodeasStep/'+selectedTestCaseId,javaCode);
}


}
