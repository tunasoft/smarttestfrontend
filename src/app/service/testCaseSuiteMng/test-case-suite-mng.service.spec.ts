import { TestBed } from '@angular/core/testing';

import { TestCaseSuiteMngService } from './test-case-suite-mng.service';

describe('TestCaseSuiteMngService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestCaseSuiteMngService = TestBed.get(TestCaseSuiteMngService);
    expect(service).toBeTruthy();
  });
});
