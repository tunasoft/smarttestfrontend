import { TestBed } from '@angular/core/testing';

import { ScheduledCasesService } from './scheduled-cases.service';

describe('ScheduledCasesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScheduledCasesService = TestBed.get(ScheduledCasesService);
    expect(service).toBeTruthy();
  });
});
