import { TestBed } from '@angular/core/testing';

import { ReportPaginatorService } from './report-paginator.service';

describe('ReportPaginatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportPaginatorService = TestBed.get(ReportPaginatorService);
    expect(service).toBeTruthy();
  });
});
