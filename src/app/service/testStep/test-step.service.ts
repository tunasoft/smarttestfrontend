import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from 'src/app/globals';
import { TestStepDetailModule } from 'src/app/models/test-step-detail/test-step-detail.module';

@Injectable({
  providedIn: 'root'
})
export class TestStepService {

  constructor(private http: HttpClient) { 

    
  }

   // get all testSteps
   getAllTestSteps() {
    return this.http.get(Globals.url + '/testStep/getAllTestSteps');
  }   

  getSomeTestSteps (pageSize:number, pageIndex:number  ){
    return this.http.get(Globals.url+"/testStep/getSomeTestStep/"+pageSize+"/"+pageIndex);
  }

  getCount(){
    return this.http.get(Globals.url+"/testStep/getCount");
  }
  getCount2(filterObject:any){
    return this.http.post(Globals.url+"/testStep/getCount2",filterObject);
  }


  getAllPlatforms(){
    return this.http.get(Globals.url + '/platform/getAllPlatforms');
  }
  deleteTestStep(id:number){
    return this.http.post(Globals.url+'/testStep/deleteTestStep',id);
  }
  saveTestStep(testStep:any){
    return this.http.post(Globals.url+'/testStep/saveTestStep',testStep);
  }
  getParametersFiltered(testStep:any){
    return this.http.post(Globals.url+'/testStep/getFiltered',testStep);
  }

  getParametersFiltered2(testStep:any,pageSize:number,pageIndex:number){
    return this.http.post(Globals.url+'/testStep/getFiltered2/'+pageSize+"/"+pageIndex,testStep);
  }

  
  saveTestStepDetail(testStepDetail:any){
    return this.http.post(Globals.url+'/testStep/details/saveTestStepDetail/'+testStepDetail.testStep.testStepId,testStepDetail);
  }
  getTestStepDetails(testStepId:any){
    return this.http.post(Globals.url+'/testStep/details/getTestStepDetails',testStepId);
  }
  getTestStepDetails2(testStepId:number){
    return this.http.get(Globals.url+'/testStep/details/getTestStepDetails2/'+testStepId);
  }
  deleteTestStepDetail(testStepDetail:TestStepDetailModule){
    return this.http.post(Globals.url+'/testStep/details/deleteTestStepDetail/'+testStepDetail.testStep.testStepId,testStepDetail);
  }
  getTestStepDetailParams(type:any){
    return this.http.post(Globals.url+'/testStep/details/params/getTestStepDetailParams',type);
  }
  saveTestStepDetailParams(testStepDetailsParams:any){
    return this.http.post(Globals.url+'/testStep/details/params/saveTestStepDetailParams',testStepDetailsParams);
  }
  getAllTestStepDetailsParams(){
    return this.http.get(Globals.url+'/testStep/details/params/getAllTestStepDetailsParams');
  }
  deleteTestStepDetailParams(testStepDetailsParams:any){
    return this.http.post(Globals.url+'/testStep/details/params/getAllTestStepDetailsParams',testStepDetailsParams)
  } 

  getTestStepsFromRelation(schedulerId:any, testCaseId:any){
    return this.http.get(Globals.url+'/testStep/getTestStepsFromRelation/'+testCaseId+'/'+schedulerId)
  }

  getUnUsedTestSteps(){
    return this.http.get(Globals.url+'/testStep/getUnUsedTestSteps')
  }
}
