import { TestBed } from '@angular/core/testing';

import { TestStepService } from './test-step.service';

describe('TestStepService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestStepService = TestBed.get(TestStepService);
    expect(service).toBeTruthy();
  });
});
