import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ScheduledCasesService {

  constructor(private http: HttpClient) { 
 
  }
  getSchedulers(){
    return this.http.get(Globals.url+"/scheduler/getSchedulers");
  }

  getSchedulerSteps(schedulerId:number){
    return this.http.get(Globals.url+'/scheduler/getSchedulerSteps/'+schedulerId);
  }

  getScheduler(id:any){
    return this.http.get(Globals.url+'/scheduler/getScheduler/'+id);
  }
  getWaitedSchedulers(){
    return this.http.get(Globals.url+'/scheduler/getWaitedSchedulers')
  }

  getFinishedSchedulers(){
    return this.http.get(Globals.url+'/scheduler/getFinishedSchedulers')
  }
  saveScheduler(scheduler:any){
    return this.http.post(Globals.url+'/scheduler/saveScheduler', scheduler)
  }
  getImageInformation(schedulerId:any){
    return this.http.get(Globals.url+'/scheduler/getImageInformation/'+schedulerId);
  }

}
