import { TestBed } from '@angular/core/testing';

import { ExecutionServiceService } from './execution-service.service';

describe('ExecutionServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExecutionServiceService = TestBed.get(ExecutionServiceService);
    expect(service).toBeTruthy();
  });
});
