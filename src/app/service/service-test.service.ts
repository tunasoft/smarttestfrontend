import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ServiceTestService {

  constructor(private http: HttpClient) { }


  getServiceTests(){
    return this.http.get(Globals.url+"/serviceTest/getServiceTests");
  }

  saveServiceTests(serviceTest:any){
    return this.http.post(Globals.url+"/serviceTest/saveServiceTests",serviceTest);
  }

}
