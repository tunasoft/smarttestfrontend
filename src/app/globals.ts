import { Injectable } from "@angular/core";

@Injectable()
export class Globals {
  static url = 'http://localhost:8080/ngta';
  static language: String = 'en';
  static languageArray:any;
}