import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/service/menu/menu.service';
import { ActivatedRoute } from '@angular/router';
import { Globals } from 'src/app/globals';
import { MatTableDataSource } from '@angular/material';
import { ServiceTestService } from 'src/app/service/service-test.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-service-test',
  templateUrl: './service-test.component.html',
  styleUrls: ['./service-test.component.css']
})
export class ServiceTestComponent implements OnInit {
  languageArray: any;
  menuItem: any=[];
  displayedColumns: string[] = ['key','value','inputoutput'];
  displayedColumns2: string[] = ['Sil','service_name','description','url'];
  serviceParameters={serviceName:"",description:"",url:"",requestType:0,request:"",requestinJson:"",parameterType:0,response:"",fullUrl:"",key:"",value:""};
  username="BARBAROS"
  password=1;
  requestOptions:any;
    dataSource: MatTableDataSource<any>;
  constructor(    private menuService: MenuService, 
    private route: ActivatedRoute,
    private serviceTestService:ServiceTestService,
    private http:HttpClient ) {
  
      this.dataSource = new MatTableDataSource<any>();
      
     }
    requests=[{id:0, name:'GET'}, {id:1, name:'POST'}];
    parameterTypes=[{id:0, name:'PathVariable'}, {id:1, name:'RequestParam'}];
  ngOnInit() {
    var headers_object = new HttpHeaders();
headers_object.append('Content-Type', 'application/json');
headers_object.append("Authorization", "Basic " + btoa("username:password"));

this.requestOptions = {
  params: new HttpParams()
};
this.requestOptions.params.set('BARBAROS', '1');
    this.languageArray = Globals.languageArray;
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data)=>{
      this.menuItem = data;

      this.menuService.loadLanguageParameters();
      let x:any={key:'0', value:'0', inputoutput:'100'}; 
      this.dataSource.data=x;

     
    

  }); 
  }

  saveServiceTest(){

    this.serviceTestService.saveServiceTests(this.serviceParameters).subscribe();
  }
  constructURL(){

    let request:any;
    let requestType:any;
    request=this.serviceParameters.request;
    requestType=this.serviceParameters.requestType;
  
    if(requestType==0){
     let splitted :any= request.split(" ",40);
     let newUrl:any=this.serviceParameters.url;
     if(this.serviceParameters.parameterType==0)
     for(let split of splitted){
      newUrl=newUrl+'/'+split;
     }
     this.serviceParameters.fullUrl=newUrl;
    }
    else if(requestType==1){
     
     let splitted :any= request.split(" ",40);
     let newUrl:any=this.serviceParameters.url;
     for(let split of splitted){
      newUrl=newUrl+'/'+split;
     }
      
     this.serviceParameters.fullUrl=newUrl;
   
     }
    
    
  }
/*
  run(){
    let request:any;
    let requestType:any;
    request=this.serviceParameters.request;
    requestType=this.serviceParameters.requestType;
    alert(requestType)
    if(requestType==0){
     let splitted :any= request.split(" ",40);
     let newUrl:any=this.serviceParameters.url;
     for(let split of splitted){
      newUrl=newUrl+'/'+split;
     }
     this.serviceParameters.fullUrl=newUrl;
    this.http.get(newUrl).subscribe((de)=>{

      this.serviceParameters.response=JSON.stringify(de);

      
    });}
    else if(requestType==1){
     
     let splitted :any= request.split(" ",40);
     let newUrl:any=this.serviceParameters.url;
     for(let split of splitted){
      newUrl=newUrl+'/'+split;
     }
      
     this.serviceParameters.fullUrl=newUrl;
    this.http.post(newUrl,JSON.parse(this.serviceParameters.requestinJson)).subscribe((x)=>{
      this.serviceParameters.response=JSON.stringify(x);

     });
    }
  }*/
  run(){
   
    let requestType:any;
 
    requestType=this.serviceParameters.requestType;
   
    if(requestType==0){
     
    
    this.http.get( this.serviceParameters.fullUrl,this.requestOptions).subscribe((de)=>{

      this.serviceParameters.response=JSON.stringify(de);

      
    });}
    else if(requestType==1){
    

    this.http.post( this.serviceParameters.fullUrl,JSON.parse(this.serviceParameters.requestinJson),this.requestOptions).subscribe((x)=>{
      this.serviceParameters.response=JSON.stringify(x);

     });
    }
  }

  addParameter(){
    if(this.serviceParameters.fullUrl.indexOf('?')>-1)
    
    
    this.serviceParameters.fullUrl=this.serviceParameters.fullUrl+"&"+this.serviceParameters.key+"="+this.serviceParameters.value;
    else
    this.serviceParameters.fullUrl=this.serviceParameters.fullUrl+"?"+this.serviceParameters.key+"="+this.serviceParameters.value;
    
  }
}
