import { Component, OnInit, Inject } from '@angular/core';
import { MenuService } from '../../service/menu/menu.service';
import { Globals } from '../../globals';
import { ActivatedRoute } from "@angular/router";
import { ParameterService } from 'src/app/service/parameter/parameter.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatTableModule, MatButton } from '@angular/material';

export interface DialogData {
  tableData: MatTableDataSource<any>,
  paramObj: any
}

@Component({
  selector: 'app-general-params',
  templateUrl: './general-params.component.html',
  styleUrls: ['./general-params.component.css']
})
export class GeneralParamsComponent implements OnInit {

  matTable: MatTableDataSource<any>;
  isDialogOpen: boolean = false;
  isDeleteOpen: boolean = false;

     
  menuItem: any = [];
  languageArray: any;
  parameterObject = {
    "gpId": null,
    "name": "",
    "value": "",
    "description": ""
  };
  filterObject = {
    "name": "",
    "value": "",
    "description": ""
  };
  Name: String = "";
  description: String = "";
  Value: String = "";
  displayedColumns: string[] = ['name', 'value', 'description', 'select', 'delete'];
  constructor(
    private menuService: MenuService,
    private route: ActivatedRoute,
    private parameterService: ParameterService,
    public dialog: MatDialog
  ) {
    this.matTable = new MatTableDataSource<any>();
  }

  ngOnInit() {
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data) => {
      this.menuItem = data;
    });
    this.menuService.loadLanguageParameters();
    this.parameterService.getAllParameters().subscribe((data) => {
      let reponse: any = data;
      this.matTable.data = reponse;
    });
  }
  showDeleteButton(){
    this.isDeleteOpen = !this.isDeleteOpen;
  }
  reset() {
    this.parameterObject.gpId = null;
    this.parameterObject.description = "";
    this.parameterObject.name = "";
    this.parameterObject.value = "";
  }

  saveParameter() {
    this.isDialogOpen = false;
    if (this.parameterObject.value != "" && this.parameterObject.name != "" && this.parameterObject.description != "") {
      this.parameterService.saveParameter(this.parameterObject).subscribe((data: any) => {
        this.matTable.data = data;
      });
    }
  }

  openDialog(): void {
    this.isDialogOpen = true;
  }
  closeDialog() {
    this.isDialogOpen = false;

  }

  filterList(){
    this.parameterService.getParametersFiltered(this.filterObject).subscribe((data:any) =>
    {
      this.matTable.data=data;
    });
  }
  filterByName(event: any): void {
    this.Name = event.target.value;
    let parameters: any = { "name": this.Name, "value": this.Value, "description": this.description };

    this.parameterService.getParametersFiltered(parameters).subscribe((res) => {
      let x: any;
      x = res;
      this.matTable.data = x;
    });
  }


  filterByValue(event: any): void {
    this.Value = event.target.value;
    let parameters: any = { "name": this.Name, "value": this.Value, "description": this.description };

    this.parameterService.getParametersFiltered(parameters).subscribe((res) => {
      let x: any;
      x = res;
      this.matTable.data = x;
    });
  }



  filterByDescription(event: any): void {
    this.description = event.target.value;
    let parameters: any = { "name": this.Name, "value": this.Value, "description": this.description };


    this.parameterService.getParametersFiltered(parameters).subscribe((res) => {
      let x: any;
      x = res;
      this.matTable.data = x
    });

  }

  onSelect(element: any): void {
    this.parameterObject.gpId = element.gpId;
    this.parameterObject.description = element.description;
    this.parameterObject.value = element.value;
    this.parameterObject.name = element.name;
    this.isDialogOpen = true;

  }
  deleteParameterById(element: any) {
    this.parameterService.deleteParameterById(element.gpId).subscribe((data: any) => {
      this.matTable.data = data;
    }
    );
  }
}