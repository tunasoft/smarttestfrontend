import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecuteReport2Component } from './execute-report2.component';

describe('ExecuteReport2Component', () => {
  let component: ExecuteReport2Component;
  let fixture: ComponentFixture<ExecuteReport2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecuteReport2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecuteReport2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
