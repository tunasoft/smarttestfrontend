import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { ParameterService } from 'src/app/service/parameter/parameter.service';
import { MenuService } from 'src/app/service/menu/menu.service';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import { ReportPaginatorService } from 'src/app/service/report-paginator.service';
import { TestStepService } from 'src/app/service/testStep/test-step.service';

@Component({
  selector: 'app-execute-report2',
  templateUrl: './execute-report2.component.html',
  styleUrls: ['./execute-report2.component.css']
})
export class ExecuteReport2Component implements OnInit {
  menuItem: any = [];
  displayedColumns: string[] = ['testCaseName','schedulerId','time','show'];
  displayedColumns2: string[] = ['testStepName','flag'];
  dataSource:MatTableDataSource<any>=new MatTableDataSource();
  dataSource2:MatTableDataSource<any>=new MatTableDataSource();
  filterObject:any={testCaseName:"", schedulerId:"", date:""}
  count: number;
  items: any[];
  count3: number;
  pageSize: number=5;
  selectedItem: number=1;
  dialogOpen=false;
  constructor( private parameterService: ParameterService,  private menuService: MenuService,private route: ActivatedRoute, private rps:ReportPaginatorService,private testStepService:TestStepService) {
    this.construct2();
   }

  ngOnInit() {

    
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data) => {
      this.menuItem = data;
    });
    this.menuService.loadLanguageParameters();
    this.rps.getFilteredReports(this.filterObject,5,0).subscribe((data)=>{
      let dataS:any;
      dataS=data;
      for(let row of dataS)
      {
        row.status=true;
      }
      this.setColor(dataS);
      this.dataSource.data=dataS;
      

    }); 
  }

  construct2(){

    this.rps.getSizeofReport(this.filterObject).subscribe((x:number)=>{
      this.count=x;
      if(this.count==0){
        this.items=[];
      }else{
        this.items=[];
      this.items.push("<")
      this.count3= (this.count/this.pageSize)+1;
      for(let i = 1 ; i< (this.count/this.pageSize)+1 ; i++){
      
      this.items.push(i);
  
  
  
  }this.items.push(">")
  }
    })
	
 
}


getPaginator(x:number){
  //this.model2.page=this.dataSource.paginator.pageIndex;
  
  this.selectedItem=x;
  this.rps.getFilteredReports(this.filterObject,this.pageSize,(x-1)*this.pageSize).subscribe((res) => {
    let data: any;
    data = res;
    for(let row of data)
    {
      row.status=true;
    }
    this.setColor(data);
    this.dataSource.data = data;

  });
  this.construct2();



}
selectChangeHandler($event){
	this.pageSize=$event.target.value;
	this.getPaginator(1);
}

filterList() {
  this.rps.getFilteredReports(this.filterObject,this.pageSize,0).subscribe((res: any) => {
    for(let row of res)
    {
      row.status=true;
    }
    this.setColor(res);
    this.dataSource.data = res;
    this.selectedItem=1;
    this.construct2();
  });
}

openDialog(){
  this.dialogOpen=true;
}

closeDialog(){
  this.dialogOpen=false;
}

getTestStepsFromRelation(report:any){
  
this.testStepService.getTestStepsFromRelation(report.schedulerId, report.testCaseId).subscribe((testSteps)=>{
  let testStepsS:any;
  testStepsS=testSteps;
  this.dataSource2.data=testStepsS;
  
}
);
}

setColor(reports:any){
 
  for(let report of reports){
  this.testStepService.getTestStepsFromRelation(report.schedulerId, report.testCaseId).subscribe((testSteps)=>{
    let testStepsS:any;
    
    testStepsS=testSteps;
    let success=true;
    for(let testStep of testStepsS){
      
      if(testStep.flag==false){
        success=false;
        break;
      } else
      success=true;
      
      
    }
    report.status=success;
    
  }
  );
  }
}
  }

