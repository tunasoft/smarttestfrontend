import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MenuService } from "src/app/service/menu/menu.service";
import { ParameterService } from "src/app/service/parameter/parameter.service";
import { MatTableDataSource } from "@angular/material";
import { TestCaseSuiteMngService } from "src/app/service/testCaseSuiteMng/test-case-suite-mng.service";
import { ScheduledCasesService } from "src/app/service/scheduled-cases.service";
import { ReportPaginatorService } from "src/app/service/report-paginator.service";

@Component({
  selector: "app-execute-report",
  templateUrl: "./execute-report.component.html",
  styleUrls: ["./execute-report.component.css"]
})
export class ExecuteReportComponent implements OnInit {
  menuItem: any = [];
  languageArray: any;
  datax: any = [];
  schedulers: any;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  schedulerSteps: any;
  displayedColumns: string[] = [
    "testCaseName",
    "browser",
    "dateTime",
    "status",
    "enviroment",
    "interval2",
    "url",
    "save",
    "show"
  ];
  testStepsDescription: any;
  schedulerObject: any;
  filterObject: any = {
    browser: "",
    dateTime: "",
    status: "",
    enviroment: "",
    interval2: "",
    url: ""
  };

  statuses = [{ id: 0, name: "Waiting" }, { id: 1, name: "Finished" }];
  count: number;
  items: any;
  count3: number;
  count2: number;
  selectedItem: number = 1;
  pageSize: any = 5;
  constructor(
    private parameterService: ParameterService,
    private menuService: MenuService,
    private testCaseService: TestCaseSuiteMngService,
    private route: ActivatedRoute,
    private schedulerService: ScheduledCasesService,
    private rps: ReportPaginatorService
  ) {
    this.startUp(1);
  }

  ngOnInit() {
    this.menuService
      .getMenuItemByPath(this.route.snapshot.routeConfig.path)
      .subscribe(data => {
        this.menuItem = data;
      });
    this.menuService.loadLanguageParameters();
  }
  startUp(pageIndex: number) {
    this.dataSource.data = this.datax;

    //this.schedulerService.getSchedulers().subscribe((schedulers:any)=>{
    this.rps
      .getParametersFiltered2(
        this.pageSize,
        (pageIndex - 1) * this.pageSize,
        this.filterObject
      )
      .subscribe((schedulers: any) => {
        for (let scheduler of schedulers) {
          this.schedulerService
            .getSchedulerSteps(scheduler.id)
            .subscribe(objects => {
              let a: any;
              a = objects;

              for (let data of a) {
                this.testCaseService.getTestCaseById(data.cId).subscribe(dd => {
                  let b: any;
                  b = dd;
                  this.schedulerService
                    .getScheduler(data.sId)
                    .subscribe(dd2 => {
                      let c: any;
                      c = dd2;

                      this.datax.push({ scheduler: c, testCase: b });
                      this.dataSource.data = this.datax;
                    });
                });
              }
              this.datax = [];
              this.construct2();
            });
        }
      });
  }

  saveStatus(data: any) {
    this.schedulerService.saveScheduler(data.scheduler).subscribe(() => {
      this.datax = [];
      this.startUp(0);
    });
  }

  construct2() {
    this.rps.getCount2(this.filterObject).subscribe((x: number) => {
      this.count = x;
      if (this.count == 0) {
        this.items = [];
      } else {
        this.items = [];
        this.items.push("<");
        this.count3 = this.count / this.pageSize + 1;
        for (let i = 1; i < this.count / this.pageSize + 1; i++) {
          this.items.push(i);
        }
        this.items.push(">");
      }
    });
  }

  selectChangeHandler($event) {
    this.pageSize = $event.target.value;
    this.getPaginator(1);
  }

  getPaginator(x: number) {
    //this.model2.page=this.dataSource.paginator.pageIndex;

    this.selectedItem = x;
    this.datax = [];
    this.startUp(x);
  }

  filterList() {
    this.selectedItem = 1;
    this.datax = [];
    this.startUp(1);
  }
}
