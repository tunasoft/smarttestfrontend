import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecuteReportComponent } from './execute-report.component';

describe('ExecuteReportComponent', () => {
  let component: ExecuteReportComponent;
  let fixture: ComponentFixture<ExecuteReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecuteReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecuteReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
