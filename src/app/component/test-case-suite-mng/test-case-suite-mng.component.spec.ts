import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestCaseSuiteMngComponent } from './test-case-suite-mng.component';

describe('TestCaseSuiteMngComponent', () => {
  let component: TestCaseSuiteMngComponent;
  let fixture: ComponentFixture<TestCaseSuiteMngComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestCaseSuiteMngComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseSuiteMngComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
