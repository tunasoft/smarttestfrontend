import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../service/menu/menu.service';
import { Globals } from '../../globals';
import { ActivatedRoute } from "@angular/router";
import { TestCaseSuiteMngService } from 'src/app/service/testCaseSuiteMng/test-case-suite-mng.service';
import { MatTree, MatTreeFlatDataSource, MatTreeNestedDataSource, MatTableDataSource, MatCheckbox } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';
import { TestStepService } from 'src/app/service/testStep/test-step.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { truncateSync } from 'fs';

interface FolderNode {
  folderId:Number;
  folderName: string;
  children?: FolderNode[];
}

@Component({
  selector: 'app-test-case-suite-mng',
  templateUrl: './test-case-suite-mng.component.html',
  styleUrls: ['./test-case-suite-mng.component.css']
})
export class TestCaseSuiteMngComponent implements OnInit {
  folderObject:any={folderId:null,parent:null, folderName:"",type:0}
  displayedColumns: string[] = ['name', 'description'];
  textObject:any={textName:'',eventType:''}
  javaText:any="package com.example.demo; import org.junit.Test; public class JavaCode2 {  @Test public void contextLoads() { System.out.print("+" \"hello\" "+");}}"
  parameterObject:any={itself:null,testCaseId:null, testCaseName:"", description:"",template:null,templateId:null};
  testSteps:any;
  menuItem:any = [];
  languageArray:any;
  root:any={folderName: ""};
  testCases:any;
  selectedFolderDetail:any;
  isFormGroupOpen:Boolean=false;
  selectedFolderId:any;
  isAddFileOpen:Boolean=false;
  selectedTestCase:any;
  d:any;
  types:any=[{name:"TestCase", id:1 },{name:"Folder", id:0 }];
 treeControl = new NestedTreeControl<FolderNode>(node => node.children);
 dataSource = new MatTreeNestedDataSource<FolderNode>();
 dataSource2= new MatTableDataSource<any>();
 dataSource3= new MatTableDataSource<any>();
 dataSource4= new MatTableDataSource<any>();
 dataObject:any={"testSteps":null,"testDetails":null};
  isDialogOpen2: boolean;
  isDialogOpen3: boolean;
  constructor(    private menuService: MenuService, private tss: TestStepService,
    private route: ActivatedRoute, private testCaseSuiteMngService: TestCaseSuiteMngService 

  ) {
    this.testCaseSuiteMngService.getRoot().subscribe((res:FolderNode[])=>{
      this.dataSource.data = res;      
    });

    
}


hasChild = (_: number, node: FolderNode) => !!node.children && node.children.length > 0;
  ngOnInit() {
    this.languageArray = Globals.languageArray;
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data)=>{
      this.menuItem = data;
    });
    this.menuService.loadLanguageParameters();
    }

    drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dataSource3.data, event.previousIndex, event.currentIndex)
    }

get(folderId:any, type:any){
  if(type==1){  
 this.testCaseSuiteMngService.getTestCase(folderId).subscribe(x=>{
   this.selectedFolderDetail=x;
   this.parameterObject.testCaseName=this.selectedFolderDetail.testCaseName;
   this.parameterObject.description=this.selectedFolderDetail.description;
   this.parameterObject.testCaseId=this.selectedFolderDetail.testCaseId;
    this.parameterObject.itself=this.selectedFolderDetail.itself;
    this.parameterObject.template=this.selectedFolderDetail.template;
    this.parameterObject.templateId=this.selectedFolderDetail.templateId;
    
    this.isFormGroupOpen=true;
    this.isAddFileOpen=false;
    this.testCaseSuiteMngService.getTestSteps(this.selectedFolderDetail.testCaseId).subscribe((res:any)=>{
      
      this.dataSource3.data=res
      
      for(let datax of this.dataSource3.data){
        this.tss.getTestStepDetails2(datax.testStepId).subscribe((x:any)=>{this.d=x;
          datax.type=true;
          
          for(let de of x)
        
        if(de.eventType==='type' || de.eventType==="select"){
        datax.type=false;
        datax.text=de.textName;
          break;
          
      }
      });
    }

    })
    
  });
}



  
}

save(){
  
 this.testCaseSuiteMngService.saveTestCase(this.parameterObject).subscribe(d=>{
  this.testCaseSuiteMngService.saveText(this.dataSource3.data).subscribe();
     
  this.testCaseSuiteMngService.getRoot().subscribe((res:FolderNode[])=>{
    this.dataSource.data = res;      
  });
  this.testCaseSuiteMngService.setOrder(this.dataSource3.data).subscribe();
  
});


}

addStep(step:any){
  alert("xyz")
}
isDialogOpen: boolean = false;
openDialog(testCase:any): void {
    this.isDialogOpen = true;
    this.selectedTestCase= testCase;

    this.tss.getAllTestSteps().subscribe((datax:any)=> {
      
      this.dataSource2.data=datax;
    });
    }
    openDialog2(testCase:any): void {
      this.isDialogOpen2 = true;
      this.selectedTestCase= testCase;
      this.testCaseSuiteMngService.getTemplatedTestCases().subscribe((datay)=>{
        let dataz:any;
        dataz=datay;
        this.dataSource4.data=dataz;


      });
      }
      openDialog3(testCase:any): void {
        this.isDialogOpen3 = true;
        this.selectedTestCase= testCase;  
      
        }
        closeDialog3() {
          this.isDialogOpen3 = false;
        }  
  closeDialog2() {
    this.isDialogOpen2 = false;
  }
  closeDialog() {
    this.isDialogOpen = false;
  }

  createFile(){
   this.testCaseSuiteMngService.saveFile(this.folderObject, this.selectedFolderId).subscribe(x=>{
    this.testCaseSuiteMngService.getRoot().subscribe((res:FolderNode[])=>{
      this.dataSource.data = res;      
    });
  });
}

  addFile(folder:any){

   this.selectedFolderId=folder.folderId;
   this.isAddFileOpen=true;
   this.isFormGroupOpen=false;
   
  }
  deleteFile(fileId:any){
    this.testCaseSuiteMngService.deleteFile(fileId).subscribe(()=>{

      this.testCaseSuiteMngService.getRoot().subscribe((res:FolderNode[])=>{
        this.dataSource.data = res;      
      });
    });
    
  }  
  addTestStep(testStep:any){
  
    this.isDialogOpen=false;
    this.testCaseSuiteMngService.addTestStep(testStep,this.selectedTestCase.testCaseId).subscribe(()=>{
      
      this.testCaseSuiteMngService.getTestSteps(this.selectedFolderDetail.testCaseId).subscribe((res:any)=>{
      
        this.dataSource3.data=res
        
        for(let datax of this.dataSource3.data){
          this.tss.getTestStepDetails2(datax.testStepId).subscribe((x:any)=>{this.d=x;
            datax.type=true;
            
            for(let de of x)
          
          if(de.eventType==='type' || de.eventType==="select"){
          datax.type=false;
          datax.text=de.textName;
            break;
            
        }
        });
      }
  
   
        this.testCaseSuiteMngService.setOrder(this.dataSource3.data).subscribe((res:any)=>{
        });
        
        
      })
      
    
    });
    
  }

  addTestCase(testCase:any){
  
    this.isDialogOpen2=false;
    this.testCaseSuiteMngService.saveTestCasesasStep(testCase.testCaseId,this.selectedTestCase.testCaseId).subscribe(()=>{
      this.testCaseSuiteMngService.getTestSteps(this.selectedFolderDetail.testCaseId).subscribe((res:any)=>{
        this.dataSource3.data=res;
        for(let datax of this.dataSource3.data){
          this.tss.getTestStepDetails2(datax.testStepId).subscribe((x:any)=>{this.d=x;
            datax.type=true;
            
            for(let de of x)
          
          if(de.eventType==='type' || de.eventType==="select"){
          datax.type=false;
          datax.text=de.textName;
            break;
            
        }
        });
      }
  
        this.testCaseSuiteMngService.setOrder(this.dataSource3.data).subscribe((res:any)=>{
        });
        
        
      })
      
    
    });

    }

    
  


  deleteStep(step:any){
    this.testCaseSuiteMngService.deleteStep(step).subscribe(()=>{

      
      this.testCaseSuiteMngService.getTestSteps(this.selectedFolderDetail.testCaseId).subscribe((res:any)=>{
        this.dataSource3.data=res;
        for(let datax of this.dataSource3.data){
          this.tss.getTestStepDetails2(datax.testStepId).subscribe((x:any)=>{this.d=x;
            datax.type=true;
            
            for(let de of x)
          
          if(de.eventType==='type' || de.eventType==="select"){
          datax.type=false;
          datax.text=de.textName;
            break;
            
        }
        });
      }
  
        this.testCaseSuiteMngService.setOrder(this.dataSource3.data).subscribe((res:any)=>{
        });
        
        
      })
      
    });
  }


  isHidden(step:any){
    alert(step.testStepId);
    return true;
  }
  
  showJavaCode(javaCode:any){
    
    this.isDialogOpen3=false;
    this.testCaseSuiteMngService.saveJavaCodeasStep(javaCode,this.selectedTestCase.testCaseId).subscribe(()=>{
      this.testCaseSuiteMngService.getTestSteps(this.selectedFolderDetail.testCaseId).subscribe((res:any)=>{
        this.dataSource3.data=res;
        for(let datax of this.dataSource3.data){
          this.tss.getTestStepDetails2(datax.testStepId).subscribe((x:any)=>{this.d=x;
            datax.type=true;
            
            for(let de of x)
          
          if(de.eventType==='type' || de.eventType==="select"){
          datax.type=false;
          datax.text=de.textName;
            break;
            
        }
        });
      }
  
        this.testCaseSuiteMngService.setOrder(this.dataSource3.data).subscribe((res:any)=>{
        });
        
        
      })
      
    
    });
  }
  
}

