import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from 'src/app/service/menu/menu.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  menuItem:any=[];

  constructor( private menuService: MenuService,
      private route: ActivatedRoute) { }

  ngOnInit() {


    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data) => {
      this.menuItem = data;
      
    });
    this.menuService.loadLanguageParameters();
  }

}
