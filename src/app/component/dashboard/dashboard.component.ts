import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from 'src/app/service/menu/menu.service';
import { TestStepService } from 'src/app/service/testStep/test-step.service';
import { ReportPaginatorService } from 'src/app/service/report-paginator.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  menuItem: any=[];
  amountOfSuccess:number=0;
  amountOfFailure:number=0;
  failureList:any=[];
  displayedColumns: string[] = ['testCaseName', 'date'];
  displayedColumns2: string[] = ['testStepName'];
  filteredFailedTestCases:any={testCaseName:"",date:""};
  dataSource:MatTableDataSource<any>=new MatTableDataSource();
  dataSource2:MatTableDataSource<any>=new MatTableDataSource();
  public pieChartLabels:string[] = ['Success', 'Failure'];
  public pieChartData:number[] = [0,0];
  public pieChartType:string = 'pie';
  public chartColors: any[] = [
    { 
      backgroundColor:["#99ff99", "#ff0000"] 
    }];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
  constructor(private menuService: MenuService,
    private route: ActivatedRoute, private testStepService:TestStepService, private rps:ReportPaginatorService) { }

  ngOnInit() {
    this.rps.getReports().subscribe((reports)=>{
      this.setAmount(reports);
  });
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data) => {
      this.menuItem = data;
      
    });
    this.menuService.loadLanguageParameters();
    this.rps.getFailedTestCases(this.filteredFailedTestCases).subscribe((filteredReports)=>

    {
    let datums:any;
    datums=filteredReports;
    this.findFilteredFailedTestCase(datums);
 
    
    })
    this.testStepService.getUnUsedTestSteps().subscribe((unUsedSteps)=>{
      let unUsedStepsS : any;
      unUsedStepsS=unUsedSteps;
      this.dataSource2.data=unUsedStepsS;
      
    })
  }


  filter(){
    this.rps.getFailedTestCases(this.filteredFailedTestCases).subscribe((filteredReports)=>

    {
      
    this.failureList=[];
    let datums:any;
    datums=filteredReports;
    this.findFilteredFailedTestCase(datums);
    this.dataSource.data=this.failureList;  
    
    });
  }

  setAmount(reports:any){
 
    for(let report of reports){
    this.testStepService.getTestStepsFromRelation(report.schedulerId, report.testCaseId).subscribe((testSteps)=>{
      let testStepsS:any;
      
      testStepsS=testSteps;
      let success=true;
      for(let testStep of testStepsS){
        
        if(testStep.flag==false){
          success=false;
          break;
        } else
        success=true;
        
        
      }
      report.status=success;
     
      if(report.status==true){
        this.amountOfSuccess++;
       
       }
       else{
       this.amountOfFailure++;
   
       
     
     }
 
     this.pieChartData= [this.amountOfSuccess,this.amountOfFailure];
     
    }
    );
    }
   


  }
  findFilteredFailedTestCase(filteredReports:any){
 
    for(let report of filteredReports){
  
    this.testStepService.getTestStepsFromRelation(report.schedulerId, report.testCaseId).subscribe((testSteps)=>{
      let testStepsS:any;
      
      testStepsS=testSteps;
      let success=true;
      for(let testStep of testStepsS){
        
        if(testStep.flag==false){
          success=false;
          break;
        } else
        success=true;
        
        
      }
      report.status=success;
     
      if(report.status==false){
       this.failureList.push(report);
  
     
     }
     this.dataSource.data=this.failureList;
    });
    
  } 

    
     
 
     
  
   
    


  }


}
