import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/service/menu/menu.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { TestCaseSuiteMngService } from 'src/app/service/testCaseSuiteMng/test-case-suite-mng.service';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatCheckbox } from '@angular/material';
import { ExecutionServiceService } from 'src/app/service/execution-service.service';

@Component({
  selector: 'app-test-execution',
  templateUrl: './test-execution.component.html',
  styleUrls: ['./test-execution.component.css']
})

export class TestExecutionComponent implements OnInit {
  treeControl = new NestedTreeControl<FolderNode>(node => node.children);
  
  selectedTestCases:any=[];
  executionObject:any={type:0,enviroment:'Server',browser:'Chrome',interval:'Immediately',dateTime:new Date(), url:'https://dmzlastmile.hepsiexpress.com:8091/#/login', s:null}
  menuItem:any= [];
  
  constructor(private menuService:MenuService, private ess:ExecutionServiceService,private router: Router,   private route: ActivatedRoute, private testCaseSuiteMngService:TestCaseSuiteMngService) { 
    this.testCaseSuiteMngService.getRoot().subscribe((res:FolderNode[])=>{
      this.dataSource.data = res;      
    });


  }

  hasChild = (_: number, node: FolderNode) => !!node.children && node.children.length > 0;
  ngOnInit() {
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data)=>{
      this.menuItem = data;
    });
  }
  
  addExcList(node:Node){
    this.selectedTestCases.push(node);
  }
  change(event:any){
    alert(event.target.value)
  }

  schedule( ){
   this.ess.addScheduler(this.executionObject,this.selectedTestCases).subscribe(()=>{
    this.router.navigate(['/scheduled-cases'])

   });

  }

  test(){
    this.ess.test().subscribe();
  }
 
  run(){
    this.ess.addScheduler(this.executionObject,this.selectedTestCases).subscribe((result:any)=>{
      this.executionObject.type=result.type;
      this.executionObject.enviroment=result.enviroment;
      this.executionObject.browser=result.browser;
      this.executionObject.interval=result.interval;
      this.executionObject.dateTime=result.dateTime;
      this.executionObject.url=result.url;
    this.executionObject.s=this.selectedTestCases;
    this.ess.run(result).subscribe(()=>{
      this.router.navigate(['/scheduled-cases'])
    });

  });
}
  browsers=[{id:0, name:'Chrome'}, {id:1, name:'Firefox'}];
  dataSource = new MatTreeNestedDataSource<FolderNode>();

create(){
  this.ess.create().subscribe();
}
clear(){
  this.ess.clear().subscribe(); 
 }

}
interface FolderNode {
  folderId:Number;
  folderName: string;
  children?: FolderNode[];
}
