import { Component, OnInit, Inject } from '@angular/core';
import { TestStepService } from 'src/app/service/testStep/test-step.service';
import { MatTableDataSource, MatSelectModule, MatSelect, MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatAutocompleteModule } from '@angular/material';
import { Globals } from 'src/app/globals';
import { DialogData } from '../general-params/general-params.component';
import { MenuService } from '../../service/menu/menu.service';
import { ActivatedRoute } from "@angular/router";
import { TestStep } from 'src/app/models/test-step/test-step.module';
import { TestStepDetailModule } from 'src/app/models/test-step-detail/test-step-detail.module';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import { TestStepDetailParameterModule } from 'src/app/models/test-step-detail-parameter/test-step-detail-parameter.module';
@Component({
  selector: 'app-test-step',
  templateUrl: './test-step.component.html',
  styleUrls: ['./test-step.component.css']
})
export class TestStepComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  detailsDataSource: MatTableDataSource<any>;
  languageArray: any;
  isDetailDialogOpen: boolean = false;
  isEditDialogOpen: boolean = false;
  
  matAutocomplete:MatAutocompleteModule = new MatAutocompleteModule;
  selectedValue: any;
 
  filterObject: any={pageName:"", platform:"", description:""};
  managementObject: TestStep;
  testStepDetailObject: TestStepDetailModule;
  testStep:TestStep;

  testStepDetailParams:TestStepDetailParameterModule;

  
  platforms: any[];
  menuItem: any = [];
  count: number;
  items: any[];
  count3: number;
  pageSize: number=5;
  selectedItem: number=1;



  po: any = {
    "pageName": "",
    "platform": "",
    "description": ""
  };

  detail = {
    "tagName": "q",
    "attributeValue": "2",
    "eventType": "3"
  };

  displayedColumns: string[] = ['pageName', 'platform', 'description', 'detail', 'delete'];
  detailsDisplayedColumns: string[] = ['tagName',  'attributeName','attributeValue', 'eventType', 'management'];

  parameterObject: any;
  isDeleteButtonOpen: boolean = false;

  constructor(
    private testStepService: TestStepService,
    private menuService: MenuService,
    public dialog: MatDialog,
    private route: ActivatedRoute) {


    testStepService.getAllPlatforms().subscribe((data: any) => {
      this.platforms = data
    });

    this.dataSource = new MatTableDataSource<any>();
  //  testStepService.getAllTestSteps().subscribe((res) => {
    testStepService.getParametersFiltered2(this.filterObject,5,0).subscribe((res) => {
      let data: any;
      data = res;
      this.dataSource.data = data;
    });
    this.languageArray = Globals.languageArray;

    this.construct2();
  }

  ngOnInit() {
    
    this.detailsDataSource = new MatTableDataSource<TestStepDetailModule>();
    this.managementObject = new TestStep;
    this.filterObject = new TestStep;
    this.testStepDetailObject = new TestStepDetailModule;
    this.testStep = new TestStep;
    this.testStepDetailParams= new TestStepDetailParameterModule(this.testStepService);
    this.testStepDetailObject.eventType
    this.menuService.loadLanguageParameters();
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data) => {
      this.menuItem = data;
    });

    this.testStepDetailParams.ngOnInit();
    //this.testStepDetailParams.itemFilter();
     
  }

  resetValues() {
    this.managementObject.description = "";
    this.managementObject.testStepId = null;
    this.managementObject.pageName = "";
    this.managementObject.platform = "";
    this.managementObject.status = null;
 

  }

  delete(testStep: any) {

    this.testStepService.deleteTestStep(testStep.testStepId).subscribe((res: any) => {
      this.dataSource.data = res;
    });

  }

  selectElement(element: any) {

    if (this.isEditDialogOpen) {
      this.managementObject.description = element.description;
      this.managementObject.pageName = element.pageName
      this.managementObject.platform = element.platform;
      this.managementObject.status = element.status;
      this.managementObject.testStepId = element.testStepId;
    }
  }
  changeDetailDialogIsShow() {
    this.testStepDetailObject.clearVaraibles();
    this.testStepDetailObject.attributeValue="";
    this.isDetailDialogOpen = !this.isDetailDialogOpen;
  
  }
  changeEditDialogIsShow() {
    this.isDetailDialogOpen = false;
    this.detailsDataSource.data = null;
    this.testStepDetailObject.clearVaraibles();
    this.isEditDialogOpen = !this.isEditDialogOpen;
  }
  changeDeleteButtonIsShow() {
    this.isDeleteButtonOpen = !this.isDeleteButtonOpen;
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(TestStepComponentDialog, {
      width: '750px',
      data: { tableData: this.dataSource.data, platforms: this.platforms }
    });

    dialogRef.afterClosed().subscribe(result => {

      this.testStepService.getAllTestSteps().subscribe((res) => {
        let data: any;
        data = res;
        this.dataSource.data = data;
      });
    });
  }

  saveTestStepDetails() {
  
    this.testStepDetailObject.testStep = this.testStep;
    this.testStepService.saveTestStepDetail(this.testStepDetailObject).subscribe((next: any) => {
    this.detailsDataSource.data = next;
     this.testStepDetailObject.clearVaraibles();  
  });
  }
  getStepDetails(element: any) {
    
    this.testStep = element;
    this.testStepService.getTestStepDetails(element).subscribe((next: any) => {
    this.detailsDataSource.data = next;
    });
  }
  deleteTestStepDetail(details:TestStepDetailModule){
    details.testStep = this.testStep;
    this.testStepService.deleteTestStepDetail(details).subscribe((next:any)=>
    {
      this.detailsDataSource.data = next;
    });
  }

  filterList() {
    this.testStepService.getParametersFiltered2(this.filterObject,this.pageSize,0).subscribe((res: any) => {
      this.dataSource.data = res;
      this.selectedItem=1;
      this.construct2();
    });
  }
  saveParameter(testStep: any) {
    this.isEditDialogOpen = false;
    testStep.kind="testStep";
    this.testStepService.saveTestStep(testStep).subscribe((data: any) => {
      this.dataSource.data = data;
    });
  }

  construct2(){

    this.testStepService.getCount2(this.filterObject).subscribe((x:number)=>{
      this.count=x;
      if(this.count==0){
        this.items=[];
      }else{
        this.items=[];
      this.items.push("<")
      this.count3= (this.count/this.pageSize)+1;
      for(let i = 1 ; i< (this.count/this.pageSize)+1 ; i++){
      
      this.items.push(i);
  
  
  
  }this.items.push(">")
  }
    })
	
 
}


getPaginator(x:number){
  //this.model2.page=this.dataSource.paginator.pageIndex;
  
  this.selectedItem=x;
  this.testStepService.getParametersFiltered2(this.filterObject,this.pageSize,(x-1)*this.pageSize).subscribe((res) => {
    let data: any;
    data = res;
    this.dataSource.data = data;
  });
  this.construct2();



}


selectChangeHandler($event){
	this.pageSize=$event.target.value;
	this.getPaginator(1);
}
}
@Component({
  selector: 'test-step-component-dialog',
  templateUrl: 'test-step-component-dialog.html',
})


export class TestStepComponentDialog {


  languageArray: any;
  po: any = {
    "pageName": "",
    "platform": "",
    "description": ""
  };

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<TestStepComponentDialog>,
    private testStepService: TestStepService) {
    this.languageArray = Globals.languageArray;
  }
 


}

