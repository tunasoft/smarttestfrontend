import { Component, OnInit, Inject } from '@angular/core';
import { MenuService } from '../../service/menu/menu.service';
import { Globals } from '../../globals';
import { ActivatedRoute } from "@angular/router";
import { ParameterService } from 'src/app/service/parameter/parameter.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatTableModule, MatButton} from '@angular/material';

export interface DialogData {
  tableData: MatTableDataSource<any>,
  paramObj:any
}

@Component({
  selector: 'app-general-params',
  templateUrl: './general-params.component.html',
  styleUrls: ['./general-params.component.css']
})
export class GeneralParamsComponent implements OnInit {

  matTable:MatTableDataSource<any>;
  
  menuItem:any = [];
  languageArray:any;
  parameterObject = {
    "gpId":null,
    "name":"",
    "value":"",
    "description":""
  };

  constructor(
    private menuService: MenuService, 
    private route: ActivatedRoute,
    private parameterService: ParameterService,
    public dialog: MatDialog
  ) {
    this.matTable = new MatTableDataSource<any>();
   }

  ngOnInit() {
    this.languageArray = Globals.languageArray;
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data)=>{
      this.menuItem = data;
    });
  }

  reset(){
    this.parameterObject.gpId=null;
    this.parameterObject.description="";
    this.parameterObject.name="";
    this.parameterObject.value="";
  }

  saveParameter(){
  if(this.parameterObject.value!="" && this.parameterObject.name!="" && this.parameterObject.description!=""){
    this.parameterService.saveParameter(this.parameterObject).subscribe((data)=>{
      let response:any = data;
    });
  }
  }

  openDialog(): void {
    this.parameterService.getAllParameters().subscribe((data)=>{
      let reponse:any = data;
      this.matTable.data = reponse;

      const dialogRef = this.dialog.open(GeneralParamsComponentDialog, {
        width: '750px',
        data: {tableData : this.matTable, paramObj: this.parameterObject}
      });

      dialogRef.afterClosed().subscribe(result => {
        this.parameterObject=result;
      });
      
    });
  }
}

@Component({
  selector: 'general-params-component-dialog',
  templateUrl: 'general-params-component-dialog.html',
})
export class GeneralParamsComponentDialog {
  
  Name:String="";
  description:String="";
  Value:String="";


  displayedColumns: string[] = ['name', 'value', 'description','select'];
  languageArray:any;
  filterObject = {
    "name":"",
    "value":"",
    "description":""
  };

  constructor(
    public dialogRef: MatDialogRef<GeneralParamsComponentDialog>,  
    @Inject(MAT_DIALOG_DATA) public data: DialogData,     private parameterService: ParameterService) {
      this.languageArray = Globals.languageArray;
      console.log(data.tableData.data);
    }

  onNoClick(): void {
    this.dialogRef.close();

  }

  filterByName(event:any): void{
    this.Name=event.target.value;
    let parameters:any={"name": this.Name,"value":this.Value,"description":this.description};

    this.parameterService.getParametersFiltered(parameters).subscribe((res)=>{
      let x:any;
      x=res;
      this.data.tableData.data=x;
    });
  }
  
 


filterByValue(event:any): void{
  this.Value=event.target.value;
  let parameters:any={"name": this.Name,"value":this.Value,"description":this.description};

  this.parameterService.getParametersFiltered(parameters).subscribe((res)=>{
    let x:any;
    x=res;
    this.data.tableData.data=x;
  });


  }



filterByDescription(event:any): void{
  this.description=event.target.value;
  let parameters:any={"name": this.Name,"value":this.Value,"description":this.description};


  this.parameterService.getParametersFiltered(parameters).subscribe((res)=>{
    let x:any;
    x=res;
    this.data.tableData.data=x});


    
 



}

onSelect(element:any):void{
this.data.paramObj.gpId=element.gpId;
this.data.paramObj.description=element.description;
this.data.paramObj.value=element.value;
this.data.paramObj.name=element.name; 


}
edit(id:number){

}

}