import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduledTestCasesComponent } from './scheduled-test-cases.component';

describe('ScheduledTestCasesComponent', () => {
  let component: ScheduledTestCasesComponent;
  let fixture: ComponentFixture<ScheduledTestCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduledTestCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduledTestCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
