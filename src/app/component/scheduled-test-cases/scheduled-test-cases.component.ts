import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/service/menu/menu.service';
import { Globals } from 'src/app/globals';
import { MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { ScheduledCasesService } from 'src/app/service/scheduled-cases.service';
import { TestCaseSuiteMngService } from 'src/app/service/testCaseSuiteMng/test-case-suite-mng.service';
import { SchedulerObjectModule } from 'src/app/models/scheduler-object/scheduler-object.module';
import { ExecutionServiceService } from 'src/app/service/execution-service.service';

@Component({
  selector: 'app-scheduled-test-cases',
  templateUrl: './scheduled-test-cases.component.html',
  styleUrls: ['./scheduled-test-cases.component.css'],


})
export class ScheduledTestCasesComponent implements OnInit {
  datax:any=[];
  languageArray: any;
  schedulers:any;
  menuItem: any=[];
  dataSource:MatTableDataSource<any>=new MatTableDataSource();
  dataSource2:MatTableDataSource<any>=new MatTableDataSource();
  schedulerSteps:any;
  displayedColumns: string[] = ['testCaseName','browser', 'dateTime','status', 'enviroment', 'interval2', 'url','save'];
  displayedColumns2: string[] = ['schedulerId','testCaseId','testStepId','testStepName','executionDate','status'];
  testStepsDescription:any;
  schedulerObject:any
  imageUrls:any;
  statuses=[{id:0, name:'Waiting'}, {id:1, name:'Finished'}];
  image: string | ArrayBuffer;
  constructor(
    
    private menuService: MenuService, 
    private route: ActivatedRoute,
    private schedulerService:ScheduledCasesService,
    private testCaseService: TestCaseSuiteMngService,
    private es: ExecutionServiceService,
    
  ) {
   
    this.startUp();
    
   }


  runWaitedSchedulers(){
    this.schedulerService.getWaitedSchedulers().subscribe((x)=>{
      let schedulers:any;
     
      schedulers=x;
      this.imageUrls=[];
      for(let scheduler of schedulers){
        this.es.run2(scheduler).subscribe(()=>{
          this.datax=[]
          this.startUp();
       
           
          this.schedulerService.getImageInformation(scheduler.id).subscribe((de)=>{
            let d:any;
            d=de;
           
            for(let imageObj of d){
             
              

       /*    this.es.getImage(imageObj.url).subscribe(byteArray=>
          {
              let ba:any;
              ba=byteArray;
              const reader = new FileReader();
              reader.readAsDataURL(new Blob([ba]));
              reader.onload = (e) => this.image =reader.result;
        


            });*/
            this.imageUrls.push(imageObj.url);
           
          }
        });
        
   


        });this.imageUrls=[this.imageUrls];
    
      }
    });

  }
  

  ngOnInit() {
  
    this.languageArray = Globals.languageArray;
    this.menuService.getMenuItemByPath(this.route.snapshot.routeConfig.path).subscribe((data)=>{
      this.menuItem = data;

      this.menuService.loadLanguageParameters();
      this.es.getImage().subscribe((data2:any[])=>{

      
      });

     
    

  }); 
 
}


startUp(){
  
//this.schedulerService.getSchedulers().subscribe((schedulers:any)=>{
  this.schedulerService.getWaitedSchedulers().subscribe((schedulers:any)=>{
    this.dataSource.data=this.datax;
   
   
  for(let scheduler of schedulers){

  
    this.schedulerService.getSchedulerSteps(scheduler.id).subscribe((objects)=>
    {
      let a:any;
      a=objects;
      
      for(let data of a){
      this.testCaseService.getTestCaseById(data.cId).subscribe((dd)=>{
      let b:any
      b=dd;
      this.schedulerService.getScheduler(data.sId).subscribe((dd2)=>{
        let c:any;
        c=dd2;
        
        this.datax.push({scheduler:c,testCase:b});
        this.dataSource.data=this.datax;
        
     
      });
    });
  }



  }); 

  

}
this.es.getTentativeReports().subscribe((tentativeReports=>{
  let tr:any;
  tr=tentativeReports;
  this.dataSource2.data=tr;
}));
});
}


saveStatus(data:any){
  this.schedulerService.saveScheduler(data.scheduler).subscribe(()=>{


    this.datax=[];
    this.startUp();
  });

}

showImages(data:any){
  this.imageUrls=[];
this.schedulerService.getImageInformation(data.scheduler.id).subscribe((imageInfos)=>{
  let imageInfoList:any;
  imageInfoList=imageInfos;
for( let imageInfo of imageInfoList)
{
  this.imageUrls.push(imageInfo.url);

}

}
);

}


getImage(){

  this.es.getImage2("xyz").subscribe((imageInfo)=>{
    

  let imageInfo2:any;
  imageInfo2=imageInfo;

  })
}

clear(){
 this.es.clear().subscribe(); 
}

}