import { Component, OnInit  } from '@angular/core';
import { MenuService } from './service/menu/menu.service';
import { Globals } from './globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private menuService: MenuService){}

  menuItems:any = [];
  subMenuItems:any = [];
  menuAccordion:any = [];

  ngOnInit() {
    // get main menu items
    this.menuService.getMenuItems().subscribe((data)=>{
      this.menuItems = data;
      this.menuAccordion = [];
      for (var i=0; i<this.menuItems.length; i++){
        this.menuAccordion.push(false);
      }
    });
    // get sub menu items
    this.menuService.getSubMenuItems().subscribe((data)=>{
      this.subMenuItems = data;
    });
    // load language parameters
    this.menuService.getLanguageParameters(Globals.language).subscribe((data)=>{
      Globals.languageArray = data;
    });
  }

  toggleAccordion(i){
    this.menuAccordion[i] = !this.menuAccordion[i];
  }
}
