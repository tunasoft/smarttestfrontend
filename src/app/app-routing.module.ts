import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralParamsComponent } from './component/general-params/general-params.component';
import { TestStepComponent } from './component/test-step/test-step.component';
import { TestCaseSuiteMngComponent } from './component/test-case-suite-mng/test-case-suite-mng.component';
import { TestExecutionComponent } from './component/test-execution/test-execution.component';
import { ScheduledTestCasesComponent } from './component/scheduled-test-cases/scheduled-test-cases.component';
import { ExecuteReport2Component } from './component/execute-report2/execute-report2.component';
import { ServiceTestComponent } from './component/service-test/service-test.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ChangePasswordComponent } from './component/change-password/change-password.component';

const routes: Routes = [{
  path:"general-params",
  component : GeneralParamsComponent
},
{ 
  path:"test-step-mng",
  component: TestStepComponent
},
{
  path:"case-suite-mng",
  component: TestCaseSuiteMngComponent
 },
 {
  path:"test-exec",
  component: TestExecutionComponent
 },
 {
   path:"scheduled-cases",
   component: ScheduledTestCasesComponent
 },
 {
  path:"execute-reports",
  component: ExecuteReport2Component
},
{
  path:"test-service",
  component: ServiceTestComponent
},{
    path:"dashboard",
  component:DashboardComponent
}
,
{
  path:"change-pass",
  component:ChangePasswordComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
