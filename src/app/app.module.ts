import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GeneralParamsComponent } from './component/general-params/general-params.component';
import { Globals } from './globals';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatTableModule,MatButtonModule, MatSelectModule, MatIconModule, MatRadioButton, MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatInputModule, MatAutocompleteModule, MatCheckboxModule, MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { TestStepComponent} from './component/test-step/test-step.component';
import { TestCaseSuiteMngComponent } from './component/test-case-suite-mng/test-case-suite-mng.component';
import {MatTreeModule} from '@angular/material/tree'
import {DragDropModule} from '@angular/cdk/drag-drop';
import { TestExecutionComponent } from './component/test-execution/test-execution.component';
import { TestStepDetailModule } from './models/test-step-detail/test-step-detail.module';
import { TestStepDetailParameterModule } from './models/test-step-detail-parameter/test-step-detail-parameter.module';
import { ScheduledTestCasesComponent } from './component/scheduled-test-cases/scheduled-test-cases.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ExecuteReportComponent } from './component/execute-report/execute-report.component';
import { ExecuteReport2Component } from './component/execute-report2/execute-report2.component';
import { ServiceTestComponent } from './component/service-test/service-test.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { ChangePasswordComponent } from './component/change-password/change-password.component';
import { UserOperationsComponent } from './component/user-operations/user-operations.component';

@NgModule({
  declarations: [
    AppComponent,
    GeneralParamsComponent,
    TestStepComponent,
    TestCaseSuiteMngComponent,
    TestExecutionComponent,
    ScheduledTestCasesComponent,
    ExecuteReportComponent,
    ExecuteReport2Component,
    ServiceTestComponent,
    DashboardComponent,
    ChangePasswordComponent,
    UserOperationsComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    MatTreeModule,
    MatIconModule,
    AngularFontAwesomeModule,DragDropModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    TestStepDetailModule,
    TestStepDetailParameterModule ,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCheckboxModule,
   ChartsModule
 
    
  ],
  entryComponents: [

  ],
  providers: [Globals,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    {provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check'}],
    
  bootstrap: [AppComponent],
  
})
export class AppModule { }
