import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { TestStepService } from 'src/app/service/testStep/test-step.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class TestStepDetailParameterModule implements OnInit {
  

  constructor(private testStepService: TestStepService){

  }




  tagNameControl = new FormControl();
  tagNameOptions: TestStepDetailParameterTableModel[]=[];
  tagNameFilteredOptions: Observable<string[]>;

  eventTypeControl = new FormControl();
  eventTypeOptions: TestStepDetailParameterTableModel[]=[];
  eventTypeFilteredOptions: Observable<string[]>;

  attributeNameControl = new FormControl();
  attributeNameOptions: TestStepDetailParameterTableModel[]=[];
  attributeNamefilteredOptions: Observable<string[]>;
  ngOnInit() {
    this.testStepService.getTestStepDetailParams("tagName").subscribe((next: any) => {
      this.tagNameOptions = next;
      
      this.testStepService.getTestStepDetailParams("attributeName").subscribe((next2: any) => {
        this.attributeNameOptions = next2;
      });
      
      this.testStepService.getTestStepDetailParams("eventType").subscribe((next3: any) => {
        this.eventTypeOptions = next3;

        this.itemFilter();
      });
    });
  }




  itemFilter() {

    this.attributeNamefilteredOptions = this.attributeNameControl.valueChanges
      .pipe(startWith(''), map((value: string) =>
        this._filter(value, this.attributeNameOptions)
      ));
    this.eventTypeFilteredOptions = this.eventTypeControl.valueChanges
      .pipe(startWith(''), map((value: string) =>
        this._filter(value, this.eventTypeOptions)
      ));
    this.tagNameFilteredOptions = this.tagNameControl.valueChanges
      .pipe(startWith(''), map((value: string) =>
        this._filter(value, this.tagNameOptions)
      ));
  }
   _filter(value: string, options: TestStepDetailParameterTableModel[]): string[] {
    let optionsArray:string[]=[];
    for(var model of options){
      optionsArray.push(model.value);
    }
    const filterValue = value.toLowerCase();

    return optionsArray.filter(option => option.toLowerCase().includes(filterValue));
  }
}
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class TestStepDetailParameterTableModel {
  id: number;
  type: string;
  value: string;
}
