import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class TestStep {

    public testStepId:number=null;
    public pageName:String="";
    public platform:String="";
    public status:number=null;
    public description:String="";
    public kind:String="";
    
 }
