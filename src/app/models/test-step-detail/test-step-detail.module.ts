import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestStep } from '../test-step/test-step.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class TestStepDetailModule { 
   id:number=null;
   tagName:String="";
   attributename:String=""
   attributeValue:String="";
   eventType:String="";
   testStep:TestStep;
   

   clearVaraibles(){
    this.id=null;
    this.tagName="";
    this.attributename="";
    this.attributeValue="";
    this.eventType="";
    this.testStep=null;
   }
}
